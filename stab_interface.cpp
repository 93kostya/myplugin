#include "stab.h"
#include "stab_interface.h"
#include <opencv2/opencv.hpp>
#include <stdio.h>
#include <iostream>
#include <string>

using namespace cv;

VideoWriter outputVideo;
Stabilization stab;

Mat frame;
bool flag = false;


extern "C"
{
	void process_frame(MyImg & img)
	{
		frame.create(img.height, img.width, CV_8UC4);
		frame.data = img.data;
		stab.proc_next_frame(frame);
		frame = stab.next_frame();
		imshow("rez", frame);
		waitKey(2);
		if (!flag)
		{
			std::string str = "compare.avi";
			outputVideo.open(str, -1 , 30.0, Size(610, 480));
			flag = outputVideo.isOpened();
			std::cout << flag << std::endl;
		}
		outputVideo.write(frame);
		
		
		img.data = frame.data;
	}
}
