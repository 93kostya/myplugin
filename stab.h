#include <opencv2/opencv.hpp>
#include <fstream>

struct Trajectory
{
    Trajectory();
    Trajectory(double _x, double _y, double _a);
	const double getX();
	const double getY();
	const double getA();

	friend const Trajectory operator+(const Trajectory & left, const Trajectory & right);
	friend const Trajectory operator-(const Trajectory & left, const Trajectory & right);
private:
	double x;
    double y;
    double a;
};

struct Stabilization
{
	static const int SMOOTHING_RADIUS = 15;
	static const int BUFFER = 2*SMOOTHING_RADIUS + 1;
	static const int HORIZONTAL_BORDER_CROP = 20;
	static const int FEATURES_TO_TRACK = 100;
	static const int CALC_FEATURES_INTERVAL = 30;


	Stabilization();
	void proc_next_frame(const cv::Mat& frame);
	cv::Mat next_frame();
	Trajectory smoothing(bool first = false);
	void calc_features();
	void calc_grey();
	void calc_trajectory();

	int ind_cur;
	int ind_prev;
	int count;
	
	cv::Mat good_features_to_track_one;
	cv::vector<cv::Point2f> good_features_to_track_one_points;
	cv::vector<cv::Mat> good_features_to_track;
	cv::vector<cv::Mat> frames_grey;
	cv::vector<cv::Mat> frames;
	cv::vector<cv::Mat> smoothed_frames;
	cv::vector<Trajectory> trajectory;
	std::ofstream smoothed_trajectory_f;
	std::ofstream original_trajectory_f;
	std::ofstream original_transform_f;
	std::ofstream smoothed_transform_f;
};