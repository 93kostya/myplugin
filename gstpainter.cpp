#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gst/gst.h>
#include <gst/video/video-format.h>

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "gstpainter.h"
#include "stab_interface.h"

GST_DEBUG_CATEGORY_STATIC (gst_painter_debug);
#define GST_CAT_DEFAULT gst_painter_debug


enum
{
	LAST_SIGNAL
};

enum
{
	PROP_0,
	PROP_SILENT
};

static GstStaticPadTemplate sink_factory = GST_STATIC_PAD_TEMPLATE ("sink",
	GST_PAD_SINK,
	GST_PAD_ALWAYS,
	GST_STATIC_CAPS ( GST_VIDEO_CAPS_MAKE ("{  BGRA }") )
	);

static GstStaticPadTemplate src_factory = GST_STATIC_PAD_TEMPLATE ("src",
	GST_PAD_SRC,
	GST_PAD_ALWAYS,
	GST_STATIC_CAPS ( GST_VIDEO_CAPS_MAKE ("{  BGRA }") )
	);

#define gst_painter_parent_class parent_class
G_DEFINE_TYPE (Gstpainter, gst_painter, GST_TYPE_ELEMENT);

static void gst_painter_set_property (GObject * object, guint prop_id,
	const GValue * value, GParamSpec * pspec);
static void gst_painter_get_property (GObject * object, guint prop_id,
	GValue * value, GParamSpec * pspec);

static gboolean gst_painter_sink_event (GstPad * pad, GstObject * parent, GstEvent * event);
static GstFlowReturn gst_painter_chain (GstPad * pad, GstObject * parent, GstBuffer * buf);

static void
	gst_painter_class_init (GstpainterClass * klass)
{
	GObjectClass *gobject_class;
	GstElementClass *gstelement_class;

	gobject_class = (GObjectClass *) klass;
	gstelement_class = (GstElementClass *) klass;

	gobject_class->set_property = gst_painter_set_property;
	gobject_class->get_property = gst_painter_get_property;

	g_object_class_install_property (gobject_class, PROP_SILENT,
		g_param_spec_boolean ("silent", "Silent", "Produce verbose output ?",
		FALSE, (GParamFlags)G_PARAM_READWRITE));

	gst_element_class_set_details_simple(gstelement_class,
		"painter",
		"Generic",
		"Element",
		"Author_name user@hostname.org");

	gst_element_class_add_pad_template (gstelement_class,
		gst_static_pad_template_get (&src_factory));
	gst_element_class_add_pad_template (gstelement_class,
		gst_static_pad_template_get (&sink_factory));
}

static void
	gst_painter_init (Gstpainter * filter)
{
	filter->sinkpad = gst_pad_new_from_static_template (&sink_factory, "sink");
	gst_pad_set_event_function (filter->sinkpad,
		GST_DEBUG_FUNCPTR(gst_painter_sink_event));
	gst_pad_set_chain_function (filter->sinkpad,
		GST_DEBUG_FUNCPTR(gst_painter_chain));
	GST_PAD_SET_PROXY_CAPS (filter->sinkpad);
	gst_element_add_pad (GST_ELEMENT (filter), filter->sinkpad);

	filter->srcpad = gst_pad_new_from_static_template (&src_factory, "src");
	GST_PAD_SET_PROXY_CAPS (filter->srcpad);
	gst_element_add_pad (GST_ELEMENT (filter), filter->srcpad);

	filter->silent = FALSE;
}

static void
	gst_painter_set_property (GObject * object, guint prop_id,
	const GValue * value, GParamSpec * pspec)
{
	Gstpainter *filter = GST_PAINTER (object);

	switch (prop_id) {
	case PROP_SILENT:
		filter->silent = g_value_get_boolean (value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
	gst_painter_get_property (GObject * object, guint prop_id,
	GValue * value, GParamSpec * pspec)
{
	Gstpainter *filter = GST_PAINTER (object);

	switch (prop_id) {
	case PROP_SILENT:
		g_value_set_boolean (value, filter->silent);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static gboolean
	gst_painter_sink_event (GstPad * pad, GstObject * parent, GstEvent * event)
{
	gboolean ret;
	Gstpainter *filter;

	filter = GST_PAINTER (parent);

	switch (GST_EVENT_TYPE (event)) {
	case GST_EVENT_CAPS:
		{
			GstCaps * caps;

			gst_event_parse_caps (event, &caps);

			GstStructure *structure = gst_caps_get_structure (caps, 0);

			gst_structure_get_int (structure, "width", &filter->width);
			gst_structure_get_int (structure, "height", &filter->height);

			filter->format = gst_structure_get_string (structure, "format");

			ret = gst_pad_event_default (pad, parent, event);
			break;
		}
	default:
		ret = gst_pad_event_default (pad, parent, event);
		break;
	}
	return ret;
}


IplImage * YUVtoRGB(IplImage * src)
{
	int w = src->width;
	int h = src->height;
	IplImage *py      = cvCreateImage(cvSize(w,    h), IPL_DEPTH_8U, 1);
    IplImage *pu      = cvCreateImage(cvSize(w/2,h/2), IPL_DEPTH_8U, 1);
    IplImage *pv      = cvCreateImage(cvSize(w/2,h/2), IPL_DEPTH_8U, 1);
    IplImage *pu_big  = cvCreateImage(cvSize(w,    h), IPL_DEPTH_8U, 1);
    IplImage *pv_big  = cvCreateImage(cvSize(w,    h), IPL_DEPTH_8U, 1);
    IplImage *image   = cvCreateImage(cvSize(w,    h), IPL_DEPTH_8U, 3);
    IplImage *result  = NULL;
	int ind = 0;
	for (int i = 0; i < w*h; ++i)
    {
        py->imageData[i] = (unsigned char) (src->imageData[ind]);
		ind++;
    }

	for (int i = 0; i < w*h/4; ++i)
    {
        pu->imageData[i] = (unsigned char) (src->imageData[ind]);
		ind++;
    }

	for (int i = 0; i < w*h/4; ++i)
    {
        pv->imageData[i] = (unsigned char) (src->imageData[ind]);
		ind++;
    }

	cvResize(pu, pu_big, CV_INTER_NN);
    cvResize(pv, pv_big, CV_INTER_NN);
    cvMerge(py, pu_big, pv_big, NULL, image);

    result = image;

	cvReleaseImage(&pu);
    cvReleaseImage(&pv);

    cvReleaseImage(&py);
    cvReleaseImage(&pu_big);
    cvReleaseImage(&pv_big);

	 if (result == NULL)
        cvReleaseImage(&image);
	 return result;
}


static GstBuffer *
	gst_painter_process_data (Gstpainter * filter, GstBuffer * buf)
{
	GstMapInfo srcmapinfo;
	gst_buffer_map (buf, &srcmapinfo, GST_MAP_READ);

	GstBuffer * outbuf = gst_buffer_new ();
	
	IplImage * dst = cvCreateImageHeader (cvSize (filter->width, filter->height), IPL_DEPTH_8U, 4);
	GstMemory * memory = gst_allocator_alloc (NULL, dst->imageSize, NULL);
	GstMapInfo dstmapinfo;
	if (gst_memory_map(memory, &dstmapinfo, GST_MAP_WRITE)) {

		memcpy (dstmapinfo.data, srcmapinfo.data, srcmapinfo.size);

		dst->imageData = (char*)dstmapinfo.data;

		MyImg myimg;
		myimg.data = (unsigned char*)dstmapinfo.data;
		myimg.width = filter->width;
		myimg.height = filter->height;
		process_frame(myimg);
		
		gst_buffer_insert_memory (outbuf, -1, memory);

		gst_memory_unmap(memory, &dstmapinfo);
	}

	cvReleaseImageHeader(&dst);
	
	gst_buffer_unmap(buf, &srcmapinfo);

	return outbuf;
}

static GstFlowReturn
	gst_painter_chain (GstPad * pad, GstObject * parent, GstBuffer * buf)
{
	Gstpainter *filter;

	filter = GST_PAINTER (parent);

	GstBuffer *outbuf;
	outbuf = gst_painter_process_data (filter, buf);

	gst_buffer_unref (buf);
	if (!outbuf) {
		GST_ELEMENT_ERROR (GST_ELEMENT (filter), STREAM, FAILED, (NULL), (NULL));
		return GST_FLOW_ERROR;
	}

	return gst_pad_push (filter->srcpad, outbuf);
}
