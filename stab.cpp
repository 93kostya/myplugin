#include "stab.h"
#include <cmath>
#include <iostream>
#include <vector>
#include <string>
#include <fstream>

using namespace cv;

Trajectory::Trajectory()
	: x(0)
	, y(0)
	, a(0)
{}

Trajectory::Trajectory(double _x, double _y, double _a)
	: x(_x)
	, y(_y)
	, a(_a)
{}

const Trajectory operator+(const Trajectory & left, const Trajectory & right)
{
	return Trajectory(left.x + right.x, left.y + right.y, left.a + right.a);
}

const Trajectory operator-(const Trajectory & left, const Trajectory & right)
{
	return Trajectory(left.x - right.x, left.y - right.y, left.a - right.a);
}

const double Trajectory::getX()
{
	return this->x;
}

const double Trajectory::getY()
{
	return this->y;
}

const double Trajectory::getA()
{
	return this->a;
}

vector<Trajectory> buffer;

Stabilization::Stabilization() 
	: ind_cur(0)
	, good_features_to_track_one_points(FEATURES_TO_TRACK)
	, smoothed_frames(BUFFER)
	, ind_prev(-1)
	, count(0)
	, good_features_to_track(BUFFER)
	, frames_grey(BUFFER)
	, frames(BUFFER) 
	, trajectory(BUFFER)
	, smoothed_trajectory_f ("smoothed_trajectory.txt")
	, original_trajectory_f ("original_trajectory.txt")
	, original_transform_f  ("original_transform.txt")
	, smoothed_transform_f  ("smoothed_transform.txt")
{}

void Stabilization::proc_next_frame(const Mat& frame)
{
	count++;

	if (ind_prev == -1)
	{
		frame.copyTo(frames[ind_cur]);
		calc_grey();
		calc_features();
		ind_prev = BUFFER - 1;
	}
	else
	{
		ind_cur = (ind_cur + 1) % BUFFER;
		ind_prev = (ind_prev + 1) % BUFFER;

		frame.copyTo(frames[ind_cur]);

		calc_grey();
		calc_features();
		calc_trajectory();
	}

}

Mat Stabilization::next_frame()
{
	if (count == 1 || count == 2)
	{
		return frames[ind_cur];
	}

	int ind_frame = 0;

	if (count <= BUFFER)
	{
		if (count % 2 == 1)
		{
			ind_frame = (count - 1)/2;
		}
		else
		{
			ind_frame = (count - 2)/2;
		}
	}
	else
	{
		ind_frame = ind_cur - SMOOTHING_RADIUS;
		if (ind_frame < 0)
			ind_frame += BUFFER;
	}

	Trajectory smoothed_point = smoothing();

	int ind_frame_prev = ind_frame - 1;
	if (ind_frame_prev < 0)
		ind_frame_prev += BUFFER;

	Trajectory new_transfom = smoothed_point - trajectory[ind_frame_prev];
	
	smoothed_transform_f << new_transfom.getX() << ";" << new_transfom.getY() << ";" << new_transfom.getA() << std::endl;

	Mat T(2, 3, CV_64F);
	T.at<double>(0,0) =  cos(new_transfom.getA());
	T.at<double>(0,1) = -sin(new_transfom.getA());
	T.at<double>(1,0) =  sin(new_transfom.getA());
	T.at<double>(1,1) =  cos(new_transfom.getA());

	T.at<double>(0,2) =  new_transfom.getX();
	T.at<double>(1,2) =  new_transfom.getY();
	
	
	Size sz = frames[ind_frame_prev].size();
	warpAffine(frames[ind_frame_prev], smoothed_frames[ind_cur], T, sz);

	return smoothed_frames[ind_cur];
}

Trajectory Stabilization::smoothing(bool first)
{
	double sum_x = 0;
	double sum_y = 0;
	double sum_a = 0;

	int len = count < BUFFER ? count : BUFFER ;

	for (int i = 0; i < len; ++i)
	{
		sum_x += trajectory[i].getX();
		sum_y += trajectory[i].getY();
		sum_a += trajectory[i].getA();
	}

	double avg_x = sum_x/len;
	double avg_y = sum_y/len;
	double avg_a = sum_a/len;

	if (count < BUFFER && count % 2 == 1)
		smoothed_trajectory_f << avg_x << ";" << avg_y << ";" << avg_a << std::endl;
	else if (count >= BUFFER)
		smoothed_trajectory_f << avg_x << ";" << avg_y << ";" << avg_a << std::endl;

	return Trajectory(avg_x, avg_y, avg_a);
}

void Stabilization::calc_features()
{
	if (count % CALC_FEATURES_INTERVAL == 1)
	{
		goodFeaturesToTrack(frames_grey[ind_cur], good_features_to_track_one, FEATURES_TO_TRACK, 0.01, 30);
		for (size_t i = 0; i < FEATURES_TO_TRACK; ++i)
		{
			Point2f pnt(good_features_to_track_one.at<float>(2*i), good_features_to_track_one.at<float>(2*i + 1));
			good_features_to_track_one_points[i] = pnt;
		}
	}
}

void Stabilization::calc_grey()
{
	cvtColor(frames[ind_cur], frames_grey[ind_cur], COLOR_BGR2GRAY);
}

void Stabilization::calc_trajectory()
{

	Mat cur_points, status, errors, cur_points2, prev_points2;
	
	calcOpticalFlowPyrLK(frames_grey[ind_prev],frames_grey[ind_cur],
		good_features_to_track_one, cur_points, status, errors);


	for (size_t i = 0; i < FEATURES_TO_TRACK; ++i)
	{
		if(status.at<char>(i))
		{
			Point2f pnt2(cur_points.at<float>(2*i), cur_points.at<float>(2*i + 1));

			prev_points2.push_back(good_features_to_track_one_points[i]);
			cur_points2.push_back(pnt2);
		}
	}
	
	Mat affine = estimateRigidTransform(prev_points2, cur_points2, true);
	double dx = 0, dy = 0, da = 0;
	
	if (affine.data)
	{
		dx = affine.at<double>(0,2);
		dy = affine.at<double>(1,2);
		da = atan2(affine.at<double>(1,0), affine.at<double>(0,0));
	}
	
	Trajectory cur_diff(dx, dy, da);
	trajectory[ind_cur] = trajectory[ind_prev] + cur_diff;
	
	original_transform_f << dx << ";" << dy << ";" << da << std::endl;
	original_trajectory_f << trajectory[ind_cur].getX() << ";" << trajectory[ind_cur].getY() << ";" << trajectory[ind_cur].getA() << std::endl;
}